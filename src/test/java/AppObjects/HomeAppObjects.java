package AppObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomeAppObjects {

    private WebDriver driver;

    public HomeAppObjects(WebDriver driver){
        this.driver = driver;
    }

    public WebElement validarConta(){
        return driver.findElement(By.xpath("//td[contains(text(),'Inter Banco')]"));
    }

    public WebElement validarValor(){
        return driver.findElement(By.xpath("//td[contains(text(),'150000.00')]"));
    }
}
