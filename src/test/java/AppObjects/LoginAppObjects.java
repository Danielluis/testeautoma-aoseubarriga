package AppObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginAppObjects {

    private WebDriver driver;

    public LoginAppObjects(WebDriver driver){
        this.driver = driver;

    }

    public WebElement email(){
        return driver.findElement(By.id("email"));
    }

    public WebElement senha(){
        return driver.findElement(By.id("senha"));
    }

    public WebElement botaoEntrer(){
        return driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
    }

    public WebElement mensagemSucesso(){
        return driver.findElement(By.xpath("//div[@class='alert alert-success']"));
    }

}
