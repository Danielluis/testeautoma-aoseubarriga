package AppObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuAppObjects {

    private WebDriver driver;

    public MenuAppObjects(WebDriver driver){
        this.driver = driver;
    }

    public WebElement contas(){
        return driver.findElement(By.xpath("//a[@class='dropdown-toggle']"));
    }

    public WebElement adicionar(){
        return driver.findElement(By.xpath("//a[contains(text(),'Adicionar')]"));
    }

    public WebElement nome(){
        return driver.findElement(By.id("nome"));
    }

    public WebElement botaoSalvar(){
        return driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
    }

    public  WebElement mensagemSucesso(){
        return driver.findElement(By.xpath("//div[@class='alert alert-success']"));
    }

    public WebElement botaoCriarMovimentacao(){
        return driver.findElement(By.xpath("//a[contains(text(),'Criar Movimentação')]"));
    }

    public WebElement botaoSalvarMovimentacao(){
        return driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
    }

    public WebElement mensagemMovimentacaoSucesso(){
        return driver.findElement(By.xpath("//div[@class='alert alert-success']"));
    }

    public WebElement botaoResumoMensal(){
        return driver.findElement(By.xpath("//a[contains(text(),'Resumo Mensal')]"));
    }

    public WebElement botaoHome(){
        return driver.findElement(By.xpath("//a[contains(text(),'Home')]"));
    }

    public WebElement contas2(){
        return driver.findElement(By.xpath("//a[@class='dropdown-toggle']"));
    }

    public WebElement lista(){
        return driver.findElement(By.xpath("//a[contains(text(),'Listar')]"));
    }

    public WebElement excluirConta(){
        return driver.findElement(By.xpath("//span[@class='glyphicon glyphicon-remove-circle']"));
    }

    public WebElement sair(){
        return driver.findElement(By.xpath("//a[contains(text(),'Sair')]"));
    }

}
