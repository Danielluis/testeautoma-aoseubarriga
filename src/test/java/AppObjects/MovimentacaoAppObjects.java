package AppObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MovimentacaoAppObjects {

    private WebDriver driver;

    public MovimentacaoAppObjects(WebDriver driver){
        this.driver = driver;
    }



    public WebElement dataMovimentacao(){
        return driver.findElement(By.id("data_transacao"));

    }

    public WebElement dataPagmento(){
        return driver.findElement(By.id("data_pagamento"));
    }

    public WebElement descricao(){
        return driver.findElement(By.id("descricao"));
    }

    public WebElement interecado(){
        return driver.findElement(By.id("interessado"));
    }
    public WebElement valor(){
        return driver.findElement(By.id("valor"));
    }

    public WebElement situacao(){
        return driver.findElement(By.id("status_pago"));
    }



}
