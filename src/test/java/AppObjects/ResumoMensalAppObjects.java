package AppObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResumoMensalAppObjects {

    private WebDriver driver;

    public ResumoMensalAppObjects(WebDriver driver){
        this.driver = driver;
    }

     public WebElement maio(){
        return driver.findElement(By.xpath("//option[contains(text(),'Maio')]"));
    }

    public WebElement ano(){
        return driver.findElement(By.xpath("//option[contains(text(),'2018')]"));
    }

    public WebElement botaoBuscar(){
        return driver.findElement(By.xpath("//input[@class='btn btn-primary']"));
    }

    public WebElement excluirMensal(){
        return driver.findElement(By.xpath("//span[@class='glyphicon glyphicon-remove-circle']"));
    }

    public WebElement mensagemExcluidoMensal(){
        return driver.findElement(By.xpath("//div[@class='alert alert-success']"));
    }


}
