package Tasks;

import AppObjects.HomeAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class HomeTasks {

    private WebDriver driver;
    private HomeAppObjects homeAppObjects;

    public HomeTasks(WebDriver driver){
        this.driver = driver;
        this.homeAppObjects = new HomeAppObjects(driver);
    }

    public void ValidacaoConta() {
        String atual = homeAppObjects.validarConta().getText();
        String expected = "Inter Banco";
        System.out.println(atual);
        Assertions.assertEquals(expected, atual);
    }
     public void ValidacaoValor(){
        String atual1 = homeAppObjects.validarValor().getText();
            String expected1= "150000.00";
            System.out.println(atual1);
         Assertions.assertEquals(expected1, atual1);


    }
}
