package Tasks;

import AppObjects.LoginAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTasks {

    private WebDriver driver;
    private LoginAppObjects loginAppObjects;

    public LoginTasks(WebDriver driver){
        this.driver = driver;
        this.loginAppObjects = new LoginAppObjects(driver);
    }

    public void Login(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='navbar-brand']")));
        loginAppObjects.email().sendKeys("daniel.luis.siqueira@gmail.com");
        loginAppObjects.senha().sendKeys("19121995");
        loginAppObjects.botaoEntrer().click();
        WebDriverWait wait1 = new WebDriverWait(driver , 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='alert alert-success']")));
        String atual = loginAppObjects.mensagemSucesso().getText();
            String expected = "Bem vindo, Daniel !";
            System.out.println(atual);
        Assertions.assertEquals(expected,atual);
    }
}
