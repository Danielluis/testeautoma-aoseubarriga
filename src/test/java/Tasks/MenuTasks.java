package Tasks;

import AppObjects.MenuAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class MenuTasks {

    private WebDriver driver;
    private MenuAppObjects menuAppObjects;

    public MenuTasks(WebDriver driver){
        this.driver = driver;
        this.menuAppObjects = new MenuAppObjects(driver);
    }

    public void Conta(){
        menuAppObjects.contas().click();
        menuAppObjects.adicionar().click();
        menuAppObjects.nome().sendKeys("Inter Banco");
        menuAppObjects.botaoSalvar().click();
        String atual = menuAppObjects.mensagemSucesso().getText();
            String expected = "Conta adicionada com sucesso!";
            System.out.println(atual);
        Assertions.assertEquals(expected, atual);

    }

    public void CriarMovimentacao(){
        menuAppObjects.botaoCriarMovimentacao().click();
    }

    public void SalvarMovimentacao(){
        menuAppObjects.botaoSalvarMovimentacao().click();
        String atual = menuAppObjects.mensagemMovimentacaoSucesso().getText();
        String expected = "Movimentação adicionada com sucesso!";
        System.out.println(atual);
        Assertions.assertEquals(expected,atual);
    }

    public void ResumoMensal(){
        menuAppObjects.botaoResumoMensal().click();
    }

    public void Home(){
        menuAppObjects.botaoHome().click();
    }

  public void ExcluirConta(){
        menuAppObjects.contas2().click();
        menuAppObjects.lista().click();
        menuAppObjects.excluirConta().click();
        menuAppObjects.sair().click();
  }

}
