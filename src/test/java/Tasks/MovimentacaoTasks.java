package Tasks;

import AppObjects.MovimentacaoAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class MovimentacaoTasks {

    private WebDriver driver;
    private MovimentacaoAppObjects movimentacaoAppObjects;

    public MovimentacaoTasks(WebDriver driver){
        this.driver = driver;
        this.movimentacaoAppObjects = new MovimentacaoAppObjects(driver);

    }

    public void Movimentacao(){

        movimentacaoAppObjects.dataMovimentacao().sendKeys("01/05/2018");
        movimentacaoAppObjects.dataPagmento().sendKeys("01/05/2018");
        movimentacaoAppObjects.descricao().sendKeys("Teste automação 13/03/2020");
        movimentacaoAppObjects.interecado().sendKeys("Daniel");
        movimentacaoAppObjects.valor().sendKeys("150000");
        movimentacaoAppObjects.situacao().click();

    }

}
