package Tasks;

import AppObjects.ResumoMensalAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResumoMensalTasks {

    private WebDriver driver;
    private ResumoMensalAppObjects resumoMensalAppObjects;

    public ResumoMensalTasks(WebDriver driver){
        this.driver = driver;
        this.resumoMensalAppObjects = new ResumoMensalAppObjects(driver);
    }

    public void ResumoMensal(){
        resumoMensalAppObjects.maio().click();
        resumoMensalAppObjects.ano().click();
        resumoMensalAppObjects.botaoBuscar().click();

    }

    public void ExcluirMenssal(){
        resumoMensalAppObjects.excluirMensal().click();
        String atual = resumoMensalAppObjects.mensagemExcluidoMensal().getText();
        String expected = "Movimentação removida com sucesso!";
        System.out.println(atual);
        Assertions.assertEquals(expected, atual);
    }
}
