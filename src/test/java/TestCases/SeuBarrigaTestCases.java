package TestCases;

import Tasks.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeuBarrigaTestCases {

    private WebDriver driver;
    private LoginTasks loginTasks;
    private MenuTasks menuTasks;
    private MovimentacaoTasks movimentacaoTasks;
    private ResumoMensalTasks resumoMensalTasks;
    private HomeTasks homeTasks;

    @BeforeEach
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.driver.manage().window().maximize();
        this.driver.get("https://seubarriga.wcaquino.me/");
        this.loginTasks = new LoginTasks(driver);
        this.menuTasks = new MenuTasks(driver);
        this.movimentacaoTasks = new MovimentacaoTasks(driver);
        this.resumoMensalTasks = new ResumoMensalTasks(driver);
        this.homeTasks = new HomeTasks(driver);
    }

    @AfterEach
    public void tearDow(){
//        driver.quit();
    }

    @Test

    public void Test(){
        loginTasks.Login();
        menuTasks.Conta();
        menuTasks.CriarMovimentacao();
        movimentacaoTasks.Movimentacao();
        menuTasks.SalvarMovimentacao();
        menuTasks.ResumoMensal();
        resumoMensalTasks.ResumoMensal();
        menuTasks.Home();
        homeTasks.ValidacaoConta();
        homeTasks.ValidacaoValor();
        menuTasks.ResumoMensal();
        resumoMensalTasks.ExcluirMenssal();
        menuTasks.ExcluirConta();


    }
}
